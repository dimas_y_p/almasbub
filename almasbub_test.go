package almasbub

import (
	"fmt"
)

// Use ToBool function to cast anytype to bool type:
func ExampleToBool_output() {
	v := ToBool(1)
	fmt.Println(v)
	// Output: true
}

// Use ToInt function to cast anytype to int type:
func ExampleToInt_output() {
	v := ToInt("1234")
	fmt.Println(v)
	// Output: 1234
}

// Use ToInt8 function to cast anytype to int8 type:
func ExampleToInt8_output() {
	v := ToInt8("1234")
	fmt.Println(v)
	// Output: 1234 (int8 type)
}

// Use ToInt16 function to cast anytype to int16 type:
func ExampleToInt16_output() {
	v := ToInt16("1234")
	fmt.Println(v)
	// Output: 1234 (int16 type)
}

// Use ToInt32 function to cast anytype to int32 type:
func ExampleToInt32_output() {
	v := ToInt32("1234")
	fmt.Println(v)
	// Output: 1234 (int32 type)
}

// Use ToInt64 function to cast anytype to int64 type:
func ExampleToInt64_output() {
	v := ToInt64("1234")
	fmt.Println(v)
	// Output: 1234 (int64 type)
}

// Use ToFloat32 function to cast anytype to float32 type:
func ExampleToFloat32_output() {
	v := ToFloat32("1234")
	fmt.Println(v)
	// Output: 1.234 (float32 type)
}

// Use ToFloat64 function to cast anytype to float64 type:
func ExampleToFloat64_output() {
	v := ToFloat64("1234")
	fmt.Println(v)
	// Output: 1.234 (float64 type)
}

// Use ToUint function to cast anytype to uint type:
func ExampleToUint_output() {
	v := ToUint("1234")
	fmt.Println(v)
	// Output: 1234 (uint type)
}

// Use ToUint8 function to cast anytype to uint8 type:
func ExampleToUint8_output() {
	v := ToUint8("1234")
	fmt.Println(v)
	// Output: 1234 (uint8 type)
}

// Use ToUint16 function to cast anytype to uint16 type:
func ExampleToUint16_output() {
	v := ToUint16("1234")
	fmt.Println(v)
	// Output: 1234 (uint16 type)
}

// Use ToUint32 function to cast anytype to uint32 type:
func ExampleToUint32_output() {
	v := ToUint32("1234")
	fmt.Println(v)
	// Output: 1234 (uint32 type)
}

// Use ToUint64 function to cast anytype to uint64 type:
func ExampleToUint64_output() {
	v := ToUint64("1234")
	fmt.Println(v)
	// Output: 1234 (uint64 type)
}

// Use ToString function to cast anytype to string type:
func ExampleToString_output() {
	v := ToString(1234)
	fmt.Println(v)
	// Output: "1234" (string type)
}

// Use ToStringDate function to cast string datetime to time type:
func ExampleToStringDate_output() {
	myTime, err := ToStringDate("2020-12-29 01:59:00")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(myTime)
	// Output: "2020-12-29 01:59:00" (string type)
}



