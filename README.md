[![Go Reference](https://pkg.go.dev/badge/rctiplus/almasbub.svg)](https://pkg.go.dev/rctiplus/almasbub)
[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=rctiplus_almasbub)

# Almasbub (translation : cast) #

* [Description](#description)
* [Importing package](#importing)
* [Documentation](#documentation)
* [FAQ](#faq)
* [License](#license)

### Description ###

Almasbub (translate : cast) is a library to provides an easy and safe datatype cast in Go. :

### Importing

Before importing almasbub into your code, first download almasbub using `go get` tool.

```bash
$ go get bitbucket.org/rctiplus/almasbub 
```

To use the almasbub, simply adding the following import statement to your `.go` files.

```go
import "bitbucket.org/rctiplus/almasbub" // with go modules disabled
```

## Documentation

Further documentation can be found on [pkg.go.dev](https://pkg.go.dev/bitbucket.org/rctiplus/almasbub)

## FAQ

**Can I contribute to make Almasbub?**

[Please do!](https://bitbucket.org/rctiplus/almasbub/blob/master/CONTRIBUTING.md) We are looking for any kind of contribution to improve Almasbub core funtionality and documentation. When in doubt, make a PR!

## License

```
Copyright (c) 2020, RCTIPlus

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```