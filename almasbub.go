// Copyright © 2020 RCTIPlus <rctiplus.com>.
// almasbub package provides an easy and safe datatype cast in Go.
package almasbub

import (
	"time"
)

// ToBool return casts of an interface to a bool type.
func ToBool(i interface{}) bool {
	v, _ := toBool(i)
	return v
}

// ToTime return casts of an interface to a time.Time type.
func ToTime(i interface{}) time.Time {
	v, _ := toTime(i)
	return v
}

// ToDuration return casts of an interface to a time.Duration type.
func ToDuration(i interface{}) time.Duration {
	v, _ := toDuration(i)
	return v
}

// ToFloat64 return casts of an interface to a float64 type.
func ToFloat64(i interface{}) float64 {
	v, _ := toFloat64(i)
	return v
}

// ToFloat32 return casts of an interface to a float32 type.
func ToFloat32(i interface{}) float32 {
	v, _ := toFloat32(i)
	return v
}

// ToInt64 return casts of an interface to an int64 type.
func ToInt64(i interface{}) int64 {
	v, _ := toInt64(i)
	return v
}

// ToInt32 return casts of an interface to an int32 type.
func ToInt32(i interface{}) int32 {
	v, _ := toInt32(i)
	return v
}

// ToInt16 return casts of an interface to an int16 type.
func ToInt16(i interface{}) int16 {
	v, _ := toInt16(i)
	return v
}

// ToInt8 return casts of an interface to an int8 type.
func ToInt8(i interface{}) int8 {
	v, _ := toInt8(i)
	return v
}

// ToInt return casts of an interface to an int type.
func ToInt(i interface{}) int {
	v, _ := toInt(i)
	return v
}

// ToUint return casts of an interface to a uint type.
func ToUint(i interface{}) uint {
	v, _ := toUint(i)
	return v
}

// ToUint64 return casts of an interface to a uint64 type.
func ToUint64(i interface{}) uint64 {
	v, _ := toUint64(i)
	return v
}

// ToUint32 return casts of an interface to a uint32 type.
func ToUint32(i interface{}) uint32 {
	v, _ := toUint32(i)
	return v
}

// ToUint16 return casts of an interface to a uint16 type.
func ToUint16(i interface{}) uint16 {
	v, _ := toUint16(i)
	return v
}

// ToUint8 return casts of an interface to a uint8 type.
func ToUint8(i interface{}) uint8 {
	v, _ := toUint8(i)
	return v
}

// ToString return casts of an interface to a string type.
func ToString(i interface{}) string {
	v, _ := toString(i)
	return v
}

// ToStringDate return casts of an string time intto a time type and error if failed.
func ToStringDate(i string) (time.Time, error) {
	v, err := stringToDate(i)
	return v, err
}